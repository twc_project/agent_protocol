# The Agent Protocol

A Window Manager Protocol for Wayland

### Description

The *Agent Protocol* is a Wayland protocol extension allowing a
compositor to except operational guidance from select clients.  The
protocol enables clients to provide window manager functionality
suitable for Desktop Environments.  This repository is part of the
[TWC Project].

Self-selected clients (called *agents*) learn compositor state through
protocol events and provide user feedback through protocol requests.
In this way, a compositor can be dynamically controlled where the
specifics of user interaction are off-loaded to independent agents.
Server look and feel is completely determined by agents, not by the
base compositor.  It is expected that agents will use GUIs to provide
user controls for window manager operation.

As an example, the workspace agent contains a request to change the
active workspace.  A user may indicate to the agent a desire to switch
workspaces.  The agent then relays this intention to the compositor
via the request.  Being a Wayland client, the agent is free to use any
means to interact with the user including a GUI.  The agent knows the
number and names of the workspaces through workspace events and may
use this information in creating its graphical images.  User input may
be from the mouse or keyboard, the UI details are up to the agent.

Agents may use well-known mechanisms such as the Wayland and xdg_shell
protocols for imaging and receiving user input.

See the comments in the protocol xml file for more detail.  See also
the [TWC Project] for more context.

#### Built-in Clients

The Wayland design discourages compositors that "leak" internal state.
Some events in this protocol do emit internal state.  For this reason,
it is recommended that Wayland clients serving as agents be deployed
as *Built-in Clients*.  That is, clients that run in the address space
and protection domain of the compositor[^1].  See the [TWC Project]
for more detail about Built-in Clients.

[^1]: Note that the Wayland Server API function
wl_display_set_global_filter() may be useful for qualifying external
clients to serve as agents.

#### Agent Overview

The Agent Protocol specifies a compositor-control API for a specific
set of window manager capabilities.  It is defined in the protocol
extension file called xdg-agent-v1.xml

The window manager abstractions available in the agent protocol
include:

 - agents and assistants
 - windows
 - window icons
 - window operations
 - interactive operations
 - server side decorations
 - workspaces
 - menus
 - keyboard shortcuts
 - compositor operations 
 - task management
 - 'parentless popups' with hotspots
 - keyboard focus transfer
 - screen awareness (for screen specific GUI content)
 - workspace awareness (for workspace specific GUI content)

#### Agents

Window management functionality is partitioned into various protocol
objects referred to as the agent interfaces.  Binding such an object
allows a client to serve as an agent of the compositor.  Clients which
bind agent interfaces are also called agents.  The following agent
interfaces are defined:

  - interactive agent

    select, move, resize

  - decoration stylist

    Supplies the surfaces for server side decorations.  Decorations
    may contain active elements for window control.

  - icon stylist

    Supplies surfaces for windows when iconified.  Images may depend
    on app_id and title.

  - menu agent

    Has the widest leeway for relaying user feedback to the
    compositor.  Handles keyboard shortcuts and orphan input.

  - workspace agent

    Switches workspaces based on user feedback.  Allows user to move
    windows among workspaces.

  - task agent

    Enables taskbars and icon managers.

In addition, the protocol supports desktop assistants and applications
which take cues from the compositor

  - desktop assistants

    Similar to agents, but don't need access to server data or state.
    Used by Desktop Environment tools, e.g., volume control or file
    browser.  Can be invoked in menus or bound to a shortcut.  Has
    access to the parentless popup mechanism.

  - desktop applications

    Using the desktop cueing system, any application can takes cues
    from the compositor.  Cues can be thought of as a suggestion,
    e.g., show settings menu.  The decoration agent uses registered
    cues to place menubar/toolbar functionality within its titlebar.
    Applications make use of the desktop_cues protocol to register
    cues and to listen for cue events.  Cue handling is most likely
    encapsulated within a Desktop Environment widget library.

#### Exclusive Singletons

The relationship between agent objects and clients is flexible.  Each
of several clients may bind one or more agent objects.  In one
extreme, all agent objects are bound by one client.  At the other
extreme, each agent object is bound by a different client.

To avoid conflicts, each agent object may be bound at most once.
Consequently, agent interfaces are not global objects, but they are
singleton objects.  The result is that the functionality of each agent
object can be accessed by only one client.  Such a singleton is known
as an *exclusive singleton*.  Notification of agent object
availability occurs only after a specific request.

#### Desktop Environments

A coherent Desktop Environment could consist of a single monolithic
client or many individual clients all sharing common patterns, designs
and themes.

However, there is no requirement that window management be provided by
related clients.  Any collection of clients may connect to a base
compositor and bind agent interfaces.  Listing agent plug-ins within a
configuration file would allow a user to exactly specify their
preferred pantheon of window manager agents.

Using one client per agent interface allows client code to have a
narrow focus, reducing size and complexity.  It becomes tractable for
an individual user to either construct an agent or modify the code of
an existing one in order to address their unique preferences.

#### Build instructions

The build process simply installs the protocol xml files in an
appropriate location.  See the [trial_howto] for instructions on how
to build the complete TWC Project.

```
    mkdir stage
    mkdir build

    PREFIX=stage

    meson setup --prefix ${PREFIX} build

    cd build
    ninja -v
    meson install
```

Installs to


```
    stage/share/wayland-protocols/experimental/xdg-agent/xdg-agent-v1.xml

    stage/pkgconfig/agent-protocol.pc
```

[TWC Project]: https://gitlab.com/twc_project
[trial_howto]: https://gitlab.com/twc_project/trial_howto/README.html
